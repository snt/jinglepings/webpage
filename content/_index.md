---
title: SNTPings
---

In 2024, we are celebrating 30 years of Studenten Net Twente.
For this occasion, we hosted another edition of *SNTPings*!

Draw on a shared canvas for everyone to see by sending ping packets as fast as possible!

This event started on <u><b>Friday November 1st</b></u> and was active throughout the weekend.

The [University of Twente][] graceously agreed to provide us with a dedicated 40Gbps link.

<style>
.yt-container {
    position: relative;
    width: 100%;
    height: 0;
    padding-bottom: 56.25%; /* 16:9 */
}
.yt-video {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
</style>

## SNTpings 2024 Timelapse
<div class="yt-container">
    <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/zS2XjYaQBok?si=ZK324pRPQnYcvhY5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share; fullscreen;" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

*Thanks to Kevin Alberts for uploading the timelapse.*

## Join the chaos

Send an IPv6 ping packet to the following address:

```
2001:610:1908:a000:<X>:<Y>:<B><G>:<R><A>
```

All values are in hexadecimal notation. And the resolution of the screen is 1920x1080 pixels.

Example: to make the pixel at (25,25) SNT Yellow (#FFD100) with 100% opacity, execute the following command:

```
ping6 2001:610:1908:a000:0019:0019:00d1:ffff
```
Note: SNTPings does not send a reply back.

Please be considerate of others.
Any kind of abuse,
like inappropriate content or or exceedingly high packet rates will lead to blacklisting of your entire `/64`.

Join us in [#snt:utwente.io][] on Matrix, or #snt on IRCnet.

## Is it opensource?
Of course it is! You can find the repository [on our Gitlab](https://gitlab.snt.utwente.nl/pings/server).

## How does it (sort of) work?
The UT has provided us with a 40 gigabit connection. This is fed from a tap on the 100g uplink to SURF and a 10G internal connection.
A SNT server with a 40gbit network card ingests incoming ICMPv6 packets using the DPDK framework.

The colors are extracted and put into an OpenCV canvas which is the rendered to a MediaMTX media server using gstreamer for further distribution.

There are two main limitations: The chosen network card (and other network equipment) is rated for 40gbps, but cannot easily keep up with the packet-per-second load. Secondary, the use of x264 software encoding significantly reduces the quality of the stream. A live-updated image with better quality is supplied [here](http://fudge.snt.utwente.nl/live.png).

Like this stuff? [Join SNT!](https://snt.utwente.nl)


## Thanks

We would like to thank Luc, Silke, Jeroen and Niels, and of course all our members for making this event possible!

[University of Twente]: https://www.utwente.nl/en/service-portal/services/lisa/
[#snt:utwente.io]:      https://matrix.to/#/#snt:utwente.io
